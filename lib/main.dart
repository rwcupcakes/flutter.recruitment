import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Medtrics Recruitment',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        // builder: (context, child) {
        //   return MediaQuery(
        //     child: child,
        //     data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
        //   );
        // },
        initialRoute: '/',
        routes: {
          '/': (BuildContext context) => HomePage(),
          '/signup': (BuildContext context) => SignUp(),
          '/signup_step_through': (BuildContext context) => SignUpStepThrough(),
        });
  }
}

class HomePage extends StatelessWidget {
  Color gradientStart = Colors.transparent;
  Color gradientEnd = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(children: <Widget>[
        ShaderMask(
          shaderCallback: (rect) {
            return LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [gradientStart, gradientEnd],
            ).createShader(
                Rect.fromLTRB(0, -140, rect.width, rect.height - 20));
          },
          blendMode: BlendMode.darken,
          child: Container(
            decoration: BoxDecoration(
              // gradient: LinearGradient(
              //     colors: [gradientStart, gradientEnd],
              //     begin: FractionalOffset(0, 0),
              //     end: FractionalOffset(0, 1),
              //     stops: [0.0, 1.0],
              //     tileMode: TileMode.clamp),
              image: DecorationImage(
                image: ExactAssetImage('assets/images/screen-1.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Column(
          children: [
            Expanded(
              child: Container(
                child: Align(
                  alignment: FractionalOffset(0.5, 0.0),
                  child: Container(
                    margin: EdgeInsets.only(top: 110.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[600],
                          blurRadius:
                              20.0, // has the effect of softening the shadow
                          spreadRadius:
                              0, // has the effect of extending the shadow
                          // offset: Offset(
                          // 10.0, // horizontal, move right 10
                          // 10.0, // vertical, move down 10
                          // ),
                        )
                      ],
                    ),
                    child: Image.asset('assets/images/Medtrics_Icon.png',
                        width: 70),
                  ),
                ),
              ),
              flex: 1,
            ),
            Expanded(
              child: Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    'Explore New Job Opportunities',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  )),
              flex: 0,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 28.0),
                child: Text(
                  'We do all the best for your future endeavors by providing the connections you need during your job seeking process.',
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                padding: EdgeInsets.symmetric(vertical: 18.0),
                constraints: BoxConstraints(
                  maxWidth: 330.0,
                ),
              ),
              flex: 0,
            ),
            Expanded(
              child: ButtonTheme(
                minWidth: 320.0,
                height: 50.0,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/signup');
                  },
                  textColor: Colors.blueAccent,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Container(
                    child: Text(
                      'Sign Up',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              flex: 0,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ButtonTheme(
                  minWidth: 320.0,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {},
                    textColor: Colors.white,
                    color: Colors.blueAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Container(
                      child: Text(
                        'Continue with Facebook',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ),
              flex: 0,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: ButtonTheme(
                  minWidth: 200.0,
                  height: 50.0,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/signup');
                    },
                    textColor: Colors.white,
                    child: Container(
                      child: Text(
                        'Log In',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ),
              flex: 0,
            ),
          ],
        ),
      ]),
    );
  }
}

class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          previousPageTitle: 'Back',
          middle: Text('Medtrics'),
        ),
        child: Stack(children: <Widget>[
          Container(
            color: Colors.white,
          ),
          Center(
              child: SafeArea(
                  child: Container(
            color: Colors.blueAccent,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                      flex: 2,
                      child: Stack(
                        children: <Widget>[
                          Center(
                              child: SafeArea(
                                  child: Container(
                            decoration: BoxDecoration(
                              // gradient: LinearGradient(
                              //     colors: [gradientStart, gradientEnd],
                              //     begin: FractionalOffset(0, 0),
                              //     end: FractionalOffset(0, 1),
                              //     stops: [0.0, 1.0],
                              //     tileMode: TileMode.clamp),
                              image: DecorationImage(
                                image:
                                    ExactAssetImage('assets/images/lmns-1.png'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ))),
                          Center(
                              child: SafeArea(
                                  child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                    Colors.transparent,
                                    Colors.grey[900]
                                  ],
                                  begin: FractionalOffset(0, 0),
                                  end: FractionalOffset(0, 1),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp),
                              // image: DecorationImage(
                              //   image:
                              //       ExactAssetImage('assets/images/lmns-1.png'),
                              //   fit: BoxFit.cover,
                              // ),
                            ),
                          ))),
                        ],
                      )),
                  Expanded(
                    flex: 4,
                    child: Container(),
                  ),
                ]),
          ))),
          SafeArea(
            child: Material(
              color: Colors.transparent,
              child: SizedBox.expand(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Container(
                          color: Colors.transparent,
                          margin: EdgeInsets.fromLTRB(30.0, 0, 0, 30.0),
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            'Sign Up',
                            style: TextStyle(
                                fontSize: 36,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40.0),
                                  topRight: Radius.circular(40.0))),
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 35.0),
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 24.0),
                                  child: Container(
                                    width: 320.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        color: Colors.black),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          labelText: 'Email'),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0),
                                child: ButtonTheme(
                                  minWidth: 320.0,
                                  height: 50.0,
                                  child: RaisedButton(
                                    // color: Theme.of(context).accentColor,
                                    color: Colors.blueAccent,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0)),
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, '/signup_step_through');
                                    },
                                    textColor: Colors.white,
                                    child: Container(
                                      child: Text(
                                        'Continue',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 20.0),
                                  child: Text(
                                      'We use email authentication.\nNo Need for a password, ever!',
                                      style:
                                          TextStyle(color: Colors.grey[600]))),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: Container(
                              color: Colors.white,
                              child: Container(
                                  margin: EdgeInsets.only(top: 20.0),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        height: 2,
                                        color: Colors.grey[100],
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 30.0),
                                        // height: 30.0,
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 30.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Container(
                                                  height: 24,
                                                  width: 24,
                                                  decoration: BoxDecoration(
                                                      color: Colors.blueAccent,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  4.0))),
                                                  child: Center(
                                                      child: Text(
                                                    'f',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ))),
                                              Container(
                                                  height: 24,
                                                  width: 24,
                                                  decoration: BoxDecoration(
                                                      color: Colors.blueAccent,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  4.0))),
                                                  child: Center(
                                                      child: Text(
                                                    't',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ))),
                                              Container(
                                                  height: 24,
                                                  width: 24,
                                                  decoration: BoxDecoration(
                                                      color: Colors.blueAccent,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  4.0))),
                                                  child: Center(
                                                      child: Text(
                                                    'Li',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ))),
                                              Container(
                                                  height: 24,
                                                  width: 24,
                                                  decoration: BoxDecoration(
                                                      color: Colors.blueAccent,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  4.0))),
                                                  child: Center(
                                                      child: Text(
                                                    'g',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ))),
                                            ],
                                          ))
                                    ],
                                  ))))
                    ]),
              ),
            ),
          ),
        ]));
  }
}

class SignUpStepThrough extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          previousPageTitle: 'Back',
          middle: Text('Sign Up'),
        ),
        child: Stack(children: <Widget>[
          Container(
            color: Colors.white,
          ),
          SafeArea(
            child: Material(
              color: Colors.blueAccent,
              child: SizedBox.expand(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                          flex: 2,
                          child: Center(
                              child: Container(
                                  margin: EdgeInsets.symmetric(vertical: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Container(
                                            child: ButtonTheme(
                                              height: 50.0,
                                              child: RaisedButton(
                                                color: Colors.deepOrange,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0)),
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, '/signup');
                                                },
                                                textColor: Colors.white,
                                                child: Container(
                                                  child: Text(
                                                    'You',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: ButtonTheme(
                                              height: 50.0,
                                              child: RaisedButton(
                                                color: Colors.deepOrange,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0)),
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, '/signup');
                                                },
                                                textColor: Colors.white,
                                                child: Container(
                                                  child: Text(
                                                    'You',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: ButtonTheme(
                                              height: 50.0,
                                              child: RaisedButton(
                                                color: Colors.deepOrange,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0)),
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, '/signup');
                                                },
                                                textColor: Colors.white,
                                                child: Container(
                                                  child: Text(
                                                    'You',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                      Container(
                                        child: Text(
                                          'What\'s your name?',
                                          style: TextStyle(
                                              fontSize: 36,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  )))),
                      Expanded(
                        flex: 4,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40.0),
                                  topRight: Radius.circular(40.0))),
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 35.0),
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 24.0),
                                  child: Container(
                                    width: 320.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        color: Colors.black),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          labelText: 'Firset name'),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0),
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 24.0),
                                  child: Container(
                                    width: 320.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        color: Colors.black),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          labelText: 'Middle name'),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0),
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 24.0),
                                  child: Container(
                                    width: 320.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        color: Colors.black),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          labelText: 'Last name'),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0),
                                child: Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 24.0),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: 151.0,
                                          height: 50.0,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Colors.black),
                                          child: TextField(
                                            decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0)),
                                                labelText: 'Title'),
                                          ),
                                        ),
                                        Container(
                                          width: 20.0,
                                          height: 50.0,
                                        ),
                                        Container(
                                          width: 151.0,
                                          height: 50.0,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Colors.black),
                                          child: TextField(
                                            decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0)),
                                                labelText: 'Title'),
                                          ),
                                        ),
                                      ],
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0),
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 24.0),
                                  child: Container(
                                    width: 320.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        color: Colors.black),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          labelText: 'Profession'),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0),
                                child: ButtonTheme(
                                  minWidth: 320.0,
                                  height: 50.0,
                                  child: RaisedButton(
                                    // color: Theme.of(context).accentColor,
                                    color: Colors.blueAccent,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0)),
                                    onPressed: () {
                                      Navigator.pushNamed(context, '/signup');
                                    },
                                    textColor: Colors.white,
                                    child: Container(
                                      child: Text(
                                        'Continue',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ),
        ]));
  }
}
